---
layout: mainPage
title: Crypto n Card Utils
image:
  feature: abstract-5.jpg
  credit: dargadgetz
  creditlink: http://www.dargadgetz.com/ios-7-abstract-wallpaper-pack-for-iphone-5-and-ipod-touch-retina/
comments: false
share: true
modified: 2017-01-20
---

<center>
    <a href="{{ site.url }}/downloads/Crypto_n_Card_Utils_v0.2.1.app.zip" class="btn btn-download">Download<br>Crypto n Card Utils v0.2.1</a>
</center>

A combination of multiple useful tools for macOS®. 

* **Cryptographic:** Hash, Encoding (Base64, HEX, ASCII), Bit operations, AES, Cipher, DES/3DES, RSA, Certificates, Password based key generation, TLV parser. 
* **Smart Card:** ATA analysis, SIM related, GSMA RSP .22 LPA

## Prerequisites
* **_Crypto n Card Utils_** will need macOS® version 10.12 or higher.
* This app is from an unidentified developer (yes, the developer of this app is not a registered [Apple Developer](https://developer.apple.com/)). To open the app anyway follow the instructions [here](https://support.apple.com/kb/PH21769?locale=en_US).  

## Bugs
As with any piece of software, **_Crypto n Card Utils_** also suffers from bugs. The developer has performed some tests and has minimal QA before a release, but, if you do find bugs help improve quality/stability, report them here.

## Features of - Crypto n Card Utils
**Cryptographic**
	
1. Hash 
 * MD2
 * MD4
 * MD5
 * SHA1
 * SHA224
 * SHA256	
 * SHA384	
 * SHA512
 * SHA3-224
 * SHA3-256
 * SHA3-384
 * SHA3-512
 * CRC16
 * CRC32 
 
2. Encode/Decode
 * Base64
 * ASCII-HEX

3. Bit operations
 * AND
 * OR
 * XOR
 * Shift left
 * Shift right
 * Rotate left
 * Rotate right

4. AES
 * AES-128
 * AES-192
 * AES-256
 * Modes (ECB/CBC/CFB/OFB)

5. Cipher
 * ChaCha20
 * Rabbit
 * Blowfish

6. DES/3DES
 * Modes (ECB/CFB)

7. Password Based Key Generation from Key+Salt

8. RSA
 * Key generation DER/PEM formats display&reading
 * Encrypt/Decrypt of messages in ASCII/HEX format
 * Sign of messages in ASCII/HEX format
 * Verify of signed messages

9. TLV parser and editor

10. Certificate Information extraction from a x509 certificate in PEM format

**Smartcard related**
Read off a Smartcard in a USB card reader

1. ATR analysis

2. SIM 
 * Read/Update EF's
 * Prepare/run scripts to perform actions on SIM card

3. MILENAGE calculations steps

4. GSMA SGP.22 - RSP v2.1 compliant eSIM information/profiles readout. Provides following functionality:
    - [x] **ES9+  (LPA -- SM-DP+)**
        1. [x] `InitiateAuthentication`
        1. [x] `GetBoundProfilePackage`
        1. [x] `AuthenticateClient`
        1. [x] `HandleNotification`
        1. [ ] `CancelSession`
    - [x] **ES10a**
        1. [x] `GetEuiccConfiguredAddresses`
        1. [x] `SetDefaultDpAddress`
    - [x] **ES10b**
        1. [x] `GetEUICCChallenge`
        1. [x] `GetEUICCInfo`
        1. [x] `PrepareDownload`
        1. [x] `LoadBoundProfilePackage`
        1. [x] `ListNotification`
        1. [x] `RetrieveNotificationsList`
        1. [x] `RemoveNotificationFromList`
        1. [ ] `LoadCRL`
        1. [x] `AuthenticateServer`
        1. [x] `CancelSession`
        1. [x] `GetRAT`
    - [x] **ES10c**
        1. [x] `GetProfilesInfo`
        1. [x] `EnableProfile`
        1. [x] `DisableProfile`
        1. [x] `DeleteProfile`
        1. [x] `eUICCMemoryReset`
        1. [x] `GetEID`
        1. [x] `SetNickname`

<center>
    <a href="{{ site.url }}/downloads/Crypto_n_Card_Utils_v0.2.1.app.zip" class="btn btn-download">Download<br>Crypto n Card Utils v0.2.1</a>
</center>

### Disclaimer
While the developer has taken precautions to validate the results as well as the stability of this software. He does not take any liability if the results are incorrect and/or if the app crashes - basically, use it at your own risk.
