---
layout: page
title: About
image:
  feature: abstract-3.jpg
  credit: dargadgetz
  creditlink: http://www.dargadgetz.com/ios-7-abstract-wallpaper-pack-for-iphone-5-and-ipod-touch-retina/
comments: false
modified: 2017-01-20
---

# About me...

**Who:**

Project manager by day, passionate developer, fun loving family man - let's just call me **_r@#_** for now. I have lived and worked in 3 continents and loved every single bit of culture & challenge that has come my way. Currently in sunny California.

**Why?**

Why **_Crypto n Card Utils_**? Well it is a combination of the notes I took/made, tools I used/wanted to - in and out of career. Though Project management keeps me busy in my day job, the developer itch takes over in my free time - so why not?

# About Crypto n Card Utils...

As described [else where](https://google.com) **_Crypto n Card Utils_** is a Swiss army knife of tools - use ’em wisely!!! 

**_Crypto n Card Utils_** is written exclusively in Apple's open sourced [Swift](https://swift.org/) language. I don't intend on reinventing the wheel so used a few of excellent libraries listed below, check it out for your personal projects. Also listed below are images/icons used in **_Crypto n Card Utils_**.

**Libraries used**

* [Carthage](https://github.com/Carthage/Carthage) as the Github page says "A simple, decentralized dependency manager for Cocoa" and it is!
* [CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift) "CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift"
* [IDZSwiftCommonCrypto](https://github.com/iosdevzone/IDZSwiftCommonCrypto) "A wrapper for Apple's Common Crypto library written in Swift."
* [SwCrypt](https://github.com/soyersoyer/SwCrypt) "RSA public/private key generation, RSA, AES encryption/decryption, RSA sign/verify in Swift with CommonCrypto in iOS and OS X"
* [Alamofire](https://github.com/Alamofire/Alamofire) "Elegant HTTP Networking in Swift"
* [Moya](https://github.com/Moya/Moya) "Network abstraction layer written in Swift."
* [Result](https://github.com/antitypical/Result) "Swift type modelling the success/failure of arbitrary operations."

**Images/Icons**

* [free-icons-download](http://www.free-icons-download.net/)
* [iconarchive](http://www.iconarchive.com/)
* [iconfinder](https://www.iconfinder.com/)